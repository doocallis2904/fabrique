## Проект Сервис уведомлений
[Техническое задание](https://www.craft.do/s/n6OVYFVUpq0o6L)

### Инструкция по установке и первому запуску 

Установить зависимости:

```bash
pip install -r requirements.txt
```

Провести миграцию:

```bash
python manage.py makemigrations
python manage.py migrate
```

Создать суперпользователя:

```bash
python manage.py createsuperuser
```

Запустить веб-сервер проекта:

```bash
python manage.py runserver
```

Запустить redis

```bash
redis-server
```

Запустить celery worker

```bash 
celery -A fabriqtest worker -l info
```

Запустить запуск задач celery по cron

```bash
celery -A fabriqtest beat -l info
```

[Описание реализованных методов в формате OpenAPI (Swagger)](http://127.0.0.1:8000/swagger/)

[Описание реализованных методов в формате OpenAPI (Redoc)](http://127.0.0.1:8000/redoc/)


## Примеры работы API
* [получение полного списка рассылок](http://127.0.0.1:8000/api/mailing/)
* [получение списка клиентов](http://127.0.0.1:8000/api/client/)
* [просмотр детальной информации по рассылке](http://127.0.0.1:8000/api/mailing/14/info/)
* [просмотр статистики по всем рассылкам](http://127.0.0.1:8000/api/mailing/stat/)
