import datetime
import pytz
from .models import Client, Mailing, Message
from rest_framework.viewsets import ModelViewSet
from .serializers import ClientSerializers, MailingSerializers, MessageSerializers, FullStatMailingSerializers
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db.models import Count, Q
from .tasks import send_mailing


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializers


class MailingViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializers

    @action(detail=True)
    def info(self, request, pk=None):
        mailing = Message.objects.filter(mailing=pk)
        serializer = MessageSerializers(mailing, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def stat(self, request):
        queryset = Mailing.objects.select_related('message').values('id').annotate(
            count_unsent=Count('status', filter=Q(message__status__in=('Fail', 'Started', 'New',))),
            count_sent=Count('status', filter=Q(message__status='Success')),
            count_all=Count('status')
        )
        serializer = FullStatMailingSerializers(queryset, many=True)
        return Response(serializer.data)
