from django.db import models
from .validators import JSONSchemaValidator
import json


class Mailing(models.Model):
    with open('main/schemas/validate_client_filter.json') as file:
        schema = json.load(file)
    start_mailing_dttm = models.DateTimeField(null=False)
    text = models.TextField(null=False)
    filter_mailing = models.JSONField(default=dict, blank=True, validators=[JSONSchemaValidator(schema)])
    finish_mailing_dttm = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=15, blank=True, default='New')


class Client(models.Model):
    phone = models.IntegerField(null=False, unique=True)
    phone_code = models.IntegerField(null=False)
    tag = models.CharField(max_length=100)
    client_tz = models.CharField(max_length=100)


class Message(models.Model):
    creation_dttm = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=15, blank=True, default='Started')
    mailing = models.ForeignKey(Mailing, on_delete=models.SET_NULL, null=True, related_name='message')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='message')
