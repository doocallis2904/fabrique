from datetime import datetime
from .models import Mailing, Client, Message
from django.db.models import Q
from fabriqtest.celery import app
import requests
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import json
import pytz


@app.task
def send_notification(client_id, mailing_id):
    client = Client.objects.get(id=client_id)
    mailing = Mailing.objects.get(id=mailing_id)
    message, created = Message.objects.get_or_create(client_id=client_id, mailing_id=mailing_id)
    message.save()
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg5NjQ3NTgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ikl2YW5EdWJjb3YifQ.97UytK5UO0Q2Mkycx5turDY5NxhWXOM1RGpg_MLJrBo'
    url = 'https://probe.fbrq.cloud/v1/send/'
    headers = {
        'Authorization': 'Bearer ' + token,
        'content-type': 'application/json'
    }
    data = json.dumps({
        "id": message.id,
        "phone": client.phone,
        "text": mailing.text,
    })
    retries = Retry(total=5, backoff_factor=2, status_forcelist=[500, 502, 503, 504])
    with requests.Session() as s:
        s.mount('http://', HTTPAdapter(max_retries=retries))
        result = s.post(url=f'{url}{message.id}', headers=headers, data=data)
    status = 'Fail' if result.status_code != 200 else 'Success'
    message.status = status
    message.save()


@app.task
def send_mailing(mailing_id):
    messages = []
    mailing = Mailing.objects.get(id=mailing_id)
    mailing.status = 'Started'
    mailing.save()
    filters = Q()
    for key, value in mailing.filter_mailing.items():
        filters &= Q(**{f'{key}': value})
    filters &= Q(status__in=['Fail', 'New'])
    clients = Client.objects.all()
    try:
        for client in clients:
            send_notification.apply_async((client.id, mailing_id),
                                          expires=mailing.finish_mailing_dttm)
    except Exception as e:
        mailing.status = 'Fail'
    else:
        mailing.status = 'Success'
    finally:
        Message.objects.bulk_update(messages, fields=['status'])
        mailing.save()


@app.task
def get_mailing():
    mailings = Mailing.objects.filter(start_mailing_dttm__lt=datetime.now(tz=pytz.UTC),
                                      finish_mailing_dttm__gt=datetime.now(tz=pytz.UTC),
                                      status__in=['Fail', 'New'])
    for mailing in mailings:
        send_mailing.apply_async((mailing.id,), expires=mailing.finish_mailing_dttm)

    Mailing.objects.filter(finish_mailing_dttm__lt=datetime.now(tz=pytz.UTC),
                           status__in=['New', 'Started']).update(status='Revoked')
