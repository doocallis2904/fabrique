from django.core.exceptions import ValidationError
from django.core.validators import BaseValidator
import jsonschema


class JSONSchemaValidator(BaseValidator):
    def compare(self, input_value, schema):
        try:
            jsonschema.validate(input_value, schema)
        except jsonschema.exceptions.ValidationError:
            raise ValidationError(f'{input_value} failed JSON schema check')
