from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, MailingViewSet


router = DefaultRouter()
router.register(r'client', ClientViewSet, basename='client')
router.register(r'mailing', MailingViewSet, basename='mailing')

urlpatterns = router.urls
