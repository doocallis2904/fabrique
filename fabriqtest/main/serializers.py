from rest_framework import serializers
from .models import Mailing, Client, Message


class MailingSerializers(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = ['start_mailing_dttm', 'text', 'filter_mailing', 'finish_mailing_dttm', 'status']


class ClientSerializers(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializers(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ['mailing', 'status', 'creation_dttm', 'client']
        depth = 1


class FullStatMailingSerializers(serializers.Serializer):
    count_unsent = serializers.IntegerField()
    count_sent = serializers.IntegerField()
    count_all = serializers.IntegerField()
    id = serializers.IntegerField()
