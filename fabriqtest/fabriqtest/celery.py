import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fabriqtest.settings')
app = Celery('fabriqtest')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.beat_schedule = {
    'start_get_mailing_every_minute': {
        'task': 'main.tasks.get_mailing',
        'schedule': crontab(minute='*'),
    }
}
